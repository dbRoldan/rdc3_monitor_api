#-*- coding: utf-8 -*-
class User(object):

    def __init__(self, id):
        self._id_user = id
        self._name = None
        self._lastname = None
        self._date_birth = None
        self._address = None
        self._email = None
        self._telephone = None
        self._pass = None

    def getId_user(self):
        return self._id_user

    def getName(self):
        return self._name

    def setName(self, name):
        self._name = name

    def getLastname(self):
        return self._lastname

    def setLastname(self, lastname):
        self._lastname = lastname

    def getDate_birth (self):
        return self._date_birth

    def setDate_birth (self, date_birth):
        self._date_birth  = date_birth

    def getEmail(self):
        return self._email

    def setEmail(self, email):
        self._email = email

    def getAddress(self):
        return self._address

    def setAddress(self, address):
        self._address = address

    def getTelephone(self):
        return self._telephone

    def setTelephone(self, telephone):
        self._telephone = telephone

    def getPass(self):
        return self._pass

    def setPass(self, pwd):
        self._pass = pwd
