# -*- coding: utf-8 -*-
import random
import json
import time
from multiprocessing import Process
import pandas as pd
from flask_socketio import SocketIO, emit
from db.connection import Connection
from flask import Flask, request, make_response, session
from utils.statistical_generation import SensorsStatistics
from controllers.sensors_data import SensorsData
from controllers.user_controller import UserController
from utils.hash_verification import Hash

# API Configuration
with open('../config/config.json') as config_file:
    config = json.load(config_file)
    port = config['port']
    port_http = config['port_http']
    reading_range = config['sensor_reading_range']
    info_dir = config['info_dir']
    db_dir = config['db_dir']

# Controllers Startup
conn = Connection(db_dir)
conn.createTables()
ctr_user = UserController(conn)

## Sensors Data
data_reader = SensorsData(port, info_dir)
def verifySensors():
    while True:
        data_reader.readSensors()
        time.sleep(reading_range)

# Server Application
app = Flask(__name__)
key_seed = 'developedbandanyone' + str(random.randint(1,100000))
app.secret_key = Hash.hash_info(key_seed)
socketio = SocketIO(app)

# Websocket construction
@socketio.on('connect_user', namespace = '/live_data')
def handle_message(connect_user):
    data_reader.readSensors()
    emit('connect', data_reader.getCurrentData())


# Aplication Routes
@app.route('/login', methods = ['POST'])
def login():
    secret = app.secret_key
    if 'token' in request.cookies:
        exist_token = request.cookies['token']
        user = ctr_user.verifyUserToken(exist_token, secret)
        if user:
                session['username'] = user['name']
                return user
        return {'code': 404, 'message': 'User not found'}
    if request.method == 'POST':
        requestJson = request.get_json(force = True)
        name = requestJson['name']
        passwd = requestJson['password']
        userVerification = ctr_user.loginUser(name, passwd, secret)
        if userVerification == 401:
            return {'code': 401, 'message': 'User Unauthorized'}
        elif userVerification == 404:
            return {'code': 404, 'message': 'User not found'}
        else:
            user, token = userVerification
            resp = make_response(user, 200)
            resp.set_cookie('token', token)
            return resp

@app.route('/sign_in', methods = ['POST'])
def sign_in():
    if request.method == 'POST':
        requestJson = request.get_json(force = True)
        pwd = requestJson['password']
        conf_password = requestJson['conf_password']
        sign = ctr_user.siginUser(requestJson)
        if  sign == 400:
            return {'code': 400, 'message': 'User exist'}
        elif sign == 406:
            return {'code': 406, 'message': 'The password does not match'}
        else:
            return {'code': 201, 'message': 'User created'}
    return {'code': 418, 'message': 'Nothing done'}

@app.route('/update/<id>', methods = ['PUT', 'GET'])
def update(id):
    if request.method == 'PUT':
        requestJson = request.get_json(force = True)
        update_user = ctr_user.updateUser(id, requestJson)
        if  update_user == 400:
            return {'code': 400, 'message': 'Password Incorrect'}
        elif update_user == 401:
            return {'code': 401, 'message': 'Password Incorrect, unauthorized'}
        elif update_user == 404:
            return {'code': 404, 'message': 'User not found'}
        else:
            return update_user



@app.route('/statistics/<sensor>')
def show_statistics(sensor):
    if 'Authorization' in request.headers:
        exist_token = request.headers['Authorization']
        user = ctr_user.verifyUserToken(exist_token, app.secret_key)
        if user:
            st_sensor = SensorsStatistics(info_dir, sensor)
            correlations = st_sensor.getCorrelations()
            dispersion = st_sensor.getDispersion_data()
            deciles = st_sensor.getDeciles_data()
            averages_day = st_sensor.getAverage_per_sample_day()
            normal_distr = st_sensor.getNormal_distr()
            report = {
                'sensor_name': sensor,
                'average': st_sensor.getAverage(),
                'trend': st_sensor.getTrend(),
                'median': st_sensor.getMedian(),
                'range':st_sensor.getRange(),
                'variance': st_sensor.getVariance(),
                'standar_deviation': st_sensor.getStandardDeviation(),
                'correlations': {
                    'max_correlation_name': correlations[0],
                    'max_correlation': correlations[1],
                    'min_correlation_name': correlations[2],
                    'min_correlation': correlations[3]
                },
                'dispersion_data': {
                    'x': dispersion[0],
                    'y': dispersion[1]
                },
                'deciles_data':{
                    'labels': deciles[0],
                    'percentages': deciles[1]
                },
                'average_per_day': {
                    'labels': averages_day[0],
                    'avgs': averages_day[1]
                },
                'normal_distribution': {
                    'x': normal_distr[0].tolist(),
                    'y': normal_distr[1].tolist()
                }
            }
            return report
    return {'code': 404, 'message': 'User not found'}

@app.route('/logout')
def logout_user():
    session.pop('username', None)
    resp = make_response({'code': 200, 'message': 'Has gone out of your account!'}, 200)
    resp.set_cookie('token', '', expires=0)
    return resp

# Application initialization
if __name__ == '__main__':
    p = Process(target = verifySensors)
    p.start()
    socketio.run(app, port = port_http, debug = True, host = '0.0.0.0')
