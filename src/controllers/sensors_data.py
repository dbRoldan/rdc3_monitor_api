# -*- coding: utf-8 -*-
import serial
import simplejson
import datetime as dt
import pandas as pd

class SensorsData(object):
    def __init__(self, port, info_dir):
        self.__info_dir = info_dir
        self.__con = serial.Serial(port, 9600)
        self.__measure_co2 = [0]
        self.__gas = [0]
        self.__humedity = [0]
        self.__temperature = [0]
        self.__light_uv = [0]
        self.__moisture = [0]
        self.__t = [0]

    def defineData(self):
        data = {
            'Time': [self.__t],
            'GAS': [self.__gas],
            'CO2': [self.__measure_co2],
            'Humedity': [self.__humedity],
            'Temperature': [self.__temperature],
            'LightUV': [self.__light_uv],
            'SoilMoisture': [self.__moisture]
        }
        return pd.DataFrame(data, columns = ['Time', 'GAS', 'CO2', 'Humedity', 'Temperature', 'LightUV', 'SoilMoisture'])

    def readSensors(self):
        jsonResult = self.__con.readline()
        try:
            print(jsonResult)
            jsonObject = simplejson.loads(jsonResult)
            self.__measure_co2 = jsonObject["measure_co2"]
            self.__gas = jsonObject["gas"]
            self.__humedity = jsonObject["humedity"]
            self.__temperature = jsonObject["temperature"]
            self.__light_uv = jsonObject["light_uv"]
            self.__moisture = jsonObject["moisture"]
            self.__t = dt.datetime.now()

            with open(self.__info_dir, mode = 'a') as f:
                self.defineData().to_csv(f, header=f.tell()==0)
        except Exception:
            pass

    def getCurrentData(self):
        data = {
            'Time': str(self.__t),
            'GAS': self.__gas,
            'CO2': self.__measure_co2,
            'Humedity': self.__humedity,
            'Temperature': self.__temperature,
            'LightUV': self.__light_uv,
            'SoilMoisture': self.__moisture
        }
        return data
