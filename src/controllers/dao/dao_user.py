#-*- coding: utf-8 -*-
import sqlite3

import sys
sys.path.append("..")
from models.user import User

class UserDAO(object):
    def __init__(self, connection):
        self.conn = connection.getConnection()
        self.cursor = connection.getCursor()

    def createUser(self, user):
        data_query = (str(user.getId_user()), user.getName(), user.getLastname(), user.getDate_birth(), user.getAddress(), user.getEmail(), str(user.getTelephone()), user.getPass())
        try:
            self.cursor.execute("INSERT INTO users VALUES (?,?,?,?,?,?,?,?)", data_query)
            self.conn.commit()
            print("TB: User Added")
            return True
        except sqlite3.OperationalError as error:
            print("USERS TB Error:", error)
            return False

    def readUserById(self, id):
        try:
            query = "SELECT * FROM users WHERE id_user = '" + str(id) + "'"
            self.cursor.execute(query)
            user_info = self.cursor.fetchone()
            if user_info is not None:
                user = User(user_info[0])
                user.setName(user_info[1])
                user.setLastname(user_info[2])
                user.setDate_birth(user_info[3])
                user.setAddress(user_info[4])
                user.setEmail(user_info[5])
                user.setTelephone(user_info[6])
                user.setPass(user_info[7])
                return user
            else:
                return None
        except sqlite3.OperationalError as error:
            print("USERS TB Error:", error)
            return None

    def readUser(self, name):
        try:
            query = "SELECT * FROM users WHERE name = '" + name + "'"
            self.cursor.execute(query)
            user_info = self.cursor.fetchone()
            if user_info is not None:
                user = User(user_info[0])
                user.setName(user_info[1])
                user.setLastname(user_info[2])
                user.setDate_birth(user_info[3])
                user.setAddress(user_info[4])
                user.setEmail(user_info[5])
                user.setTelephone(user_info[6])
                user.setPass(user_info[7])
                return user
            else:
                return None
        except sqlite3.OperationalError as error:
            print("USERS TB Error:", error)
            return None

    def updateUser(self, user):
        try:
            data_query = (user.getName(), user.getLastname(), user.getDate_birth(), user.getAddress(), user.getEmail(), user.getTelephone(), user.getPass(), user.getId_user())
            query = 'UPDATE users SET name = ?, lastname = ?, date_birth = ?, address = ?, email = ?, telephone = ?, password = ? WHERE id_user = ?'
            self.cursor.execute(query, data_query)
            self.conn.commit()
            return True
        except sqlite3.OperationalError as error:
            print("USER TB Error:", error)
            return False
