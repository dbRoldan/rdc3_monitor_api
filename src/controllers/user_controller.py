#-*- coding: utf-8 -*-
import jwt
import sys
import datetime as dt
from controllers.dao.dao_user import UserDAO
from utils.hash_verification import Hash
sys.path.append("..")
from models.user import User

class UserController(object):

    def __init__(self, conn):
        self.ctr_user = UserDAO(conn)

    def siginUser(self, requestJson):
        pwd = requestJson['password']
        conf_password = requestJson['conf_password']
        if pwd == conf_password:
            name = requestJson['name']
            if not self.ctr_user.readUser(name):
                id_user = requestJson['id_user']
                if not self.ctr_user.readUserById(id_user):
                    user = User(id_user)
                    user.setName(name)
                    user.setLastname(requestJson['lastname'])
                    user.setDate_birth(requestJson['date_birth'])
                    user.setAddress(requestJson['address'])
                    user.setEmail(requestJson['email'])
                    user.setTelephone(requestJson['telephone'])
                    user.setPass(Hash.hash_info(pwd))
                    return self.ctr_user.createUser(user)
            return 400
        return 406


    def loginUser(self, name, password, secret):
        exact_time = dt.datetime.now()
        user = self.ctr_user.readUser(name)
        if user is not None:
            if Hash.verify_two_hash(user.getPass(), password):
                info = {'id': user.getId_user(), 'email': user.getEmail(), 'actual_time': str(dt.date.today()) + str(exact_time.hour)}
                token = jwt.encode(info, secret, algorithm='HS256')
                return self.userToJson(user), token
            return 401
        return 404

    def updateUser(self, id, requestJson):
        user = self.ctr_user.readUserById(id)
        passwd = requestJson['password']
        if user:
            if Hash.verify_two_hash(user.getPass(), passwd):
                user = User(id)
                user.setName(requestJson['name'])
                user.setLastname(requestJson['lastname'])
                user.setDate_birth(requestJson['date_birth'])
                user.setAddress(requestJson['address'])
                user.setEmail(requestJson['email'])
                user.setTelephone(requestJson['telephone'])
                if 'new_password' in requestJson:
                    new_pwd = requestJson['new_password']
                    if new_pwd != '':
                        conf_password = requestJson['conf_password']
                        if new_pwd == conf_password:
                            user.setPass(Hash.hash_info(new_pwd))
                        else:
                            return 400                    
                user.setPass(Hash.hash_info(passwd))
                if self.ctr_user.updateUser(user):
                    return self.userToJson(self.ctr_user.readUserById(id))
                return None
            else:
                return 401
        else:
            return 404

    def verifyUserToken(self, token, secret):
        info_token = jwt.decode(token, secret, algorithms=['HS256'])
        id_user = info_token['id']
        email = info_token['email']
        login_time = info_token['actual_time']
        exact_time = dt.datetime.now()
        actual_time = str(dt.date.today()) + str(exact_time.hour)
        user = self.ctr_user.readUserById(id_user)
        if user is not None:
            if user.getEmail() == email and login_time == actual_time:
                return self.userToJson(user)

    def userToJson(self, user):
        return {
            "id_user": user.getId_user(),
            "name": user.getName(),
            "lastname": user.getLastname(),
            "date_birth": user.getDate_birth(),
            "address": user.getAddress(),
            "email": user.getEmail(),
            "telephone": user.getTelephone()
        }
