#-*- coding: utf-8 -*-
import sqlite3

class Connection(object):

    def __init__(self, db_dir):
        try:
            self.connection = sqlite3.connect(db_dir, check_same_thread = False)
            self.cursor = self.connection.cursor()
            print('DB: Database Conected')
        except sqlite3.OperationalError as error:
            print('Error:', error)

    def createTables(self):
        try:
            self.cursor.execute('CREATE TABLE IF NOT EXISTS users(id_user TEXT PRIMARY KEY, name TEXT, lastname TEXT, date_birth TEXT, address TEXT, email TEXT, telephone TEXT, password TEXT)')
            print('DB: Users Table created')
        except sqlite3.OperationalError as error:
            print('DB Error:', error)

    def getConnection(self):
        return self.connection

    def getCursor(self):
        return self.cursor

    def closeConnection():
        self.connection.close()
        print('DB: OFFLINE DataBase')

    def getUsers(self):
        return self.cursor.execute('SELECT * FROM users')
