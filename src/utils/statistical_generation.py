import numpy as np
import pandas as pd
import statistics as st
import datetime as dt
from scipy import stats

class SensorsStatistics(object):
    def __init__(self, info_dir, sensor):
        self.data_sensor = pd.read_csv(info_dir)
        self.sensor = sensor
    ## Statistical measures
    # Medidas de tendencia central
    def getAverage(self):
        return st.mean(self.data_sensor[self.sensor].tolist())

    def getTrend(self):
        return st.mode(self.data_sensor[self.sensor].tolist())

    def getMedian(self):
        return st.median(self.data_sensor[self.sensor].tolist())

    # Dispersion
    def getRange(self):
        array = self.data_sensor[self.sensor].tolist()
        return max(array) - min(array)

    def getVariance(self):
        return st.variance(self.data_sensor[self.sensor].tolist())

    def getStandardDeviation(self):
        return np.std(self.data_sensor[self.sensor].tolist())

    #Relacion entre variables
    def getCorrelations(self):
        label_corr = []
        correlations = []
        data = self.data_sensor[self.sensor].tolist()
        cols = self.data_sensor.columns.tolist()
        cols.pop(0)
        cols.pop(0)
        for c in cols:
            if c != self.sensor:
                data_aux = self.data_sensor[c].tolist()
                label_corr.append(c)
                correlations.append(np.corrcoef(data, data_aux)[0][1])
        corr_aux = list(map(abs, correlations))
        index_max = corr_aux.index(max(corr_aux))
        index_min = corr_aux.index(min(corr_aux))
        max_correlation_name = label_corr[index_max]
        max_correlation = correlations[index_max]
        min_correlation_name = label_corr[index_min]
        min_correlation = correlations[index_min]
        return max_correlation_name, max_correlation, min_correlation_name, min_correlation


    ##Graphics
    def getDispersion_data(self):
        x = self.data_sensor['Time'].tolist(),
        y = self.data_sensor[self.sensor].tolist()
        return x, y

    def get_percentage(self, total, array):
        return 100 * len(array) / total

    def getDeciles_data(self):
        data = self.data_sensor[self.sensor].tolist()
        total_data = len(data)
        range_deciles = np.percentile(data, np.arange(0, 100, 10))
        sizes = []
        labels = []
        for i in range(len(range_deciles)):
            if (i+1) < len(range_deciles):
                next_comp = range_deciles[i+1]
            else:
                next_comp = max(data)
            data_aux_min =  self.data_sensor[self.sensor][self.data_sensor[self.sensor] > range_deciles[i]]
            data_aux = data_aux_min[ self.data_sensor[self.sensor] <= next_comp]
            sizes.append(self.get_percentage(total_data, data_aux.tolist()))
            labels.append(str(range_deciles[i])+'-'+str(next_comp))
        return labels, sizes

    def format_time(self, time):
        return dt.datetime.strptime(time, '%Y-%m-%d %H:%M:%S.%f')

    def getAverage_per_sample_day(self):
        labels, avgs = [], []
        times = self.data_sensor['Time'].tolist()
        data = list(map(dt.datetime.date, list(map(self.format_time, times))))
        days = list(dict.fromkeys(data))
        self.data_sensor['Time'] = pd.to_datetime(self.data_sensor['Time'])
        days.append(days[len(days)-1] + dt.timedelta(days=1))
        min_time = dt.datetime.min.time()
        for i in range(len(days)-1):
            datetime_min = dt.datetime.combine(days[i], min_time)
            datetime_max = dt.datetime.combine(days[i+1], min_time)
            mask = (self.data_sensor['Time'] >= datetime_min) & (self.data_sensor['Time'] < datetime_max)
            df = self.data_sensor.loc[mask]
            labels.append(str(days[i]))
            avgs.append(df[self.sensor].sum()/df[self.sensor].count())
        return labels, avgs

    def getNormal_distr(self):
        mu, sigma = self.getAverage(), self.getStandardDeviation()
        normal = stats.norm(mu, sigma)
        x = np.linspace(normal.ppf(0.01),
                        normal.ppf(0.99), 100)
        fp = normal.pdf(x)
        return x, fp
